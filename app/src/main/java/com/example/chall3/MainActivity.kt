package com.example.chall3

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        // Inisialisasi data makanan
        val foodList = listOf(
            FoodItem("Burger", "Rp 30.000", "Rp 30.000", R.drawable.burger, "Alamat Restoran 1"),
            FoodItem("Mie Goreng", "Rp 25.000", "Rp 25.000", R.drawable.miegoreng, "Alamat Restoran 2"),
            FoodItem("Snack", "Rp 15.000", "Rp 15.000", R.drawable.snack, "Alamat Restoran 3"),
            FoodItem("Minuman", "Rp 10.000", "Rp 10.000", R.drawable.minuman, "Alamat Restoran 4")
        )
    }
}


