package com.example.chall3

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.chall3.FoodItem
import com.example.chall3.R
import android.net.Uri
import android.content.Intent

class FoodAdapter(private val menuItems: List<FoodItem>, private val onItemClick: (FoodItem) -> Unit) :
    RecyclerView.Adapter<FoodAdapter.FoodViewHolder>() {

    // Click listener interface
    interface OnItemClickListener {
        fun onItemClick(foodItem: FoodItem)
    }

    private var onItemClickListener: OnItemClickListener? = null

    fun setOnItemClickListener(listener: OnItemClickListener) {
        onItemClickListener = listener
    }

    inner class FoodViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val foodName: TextView = itemView.findViewById(R.id.foodName)
        val foodDescription: TextView = itemView.findViewById(R.id.foodDescription)
        val foodPrice: TextView = itemView.findViewById(R.id.foodPrice)
        val foodImage: ImageView = itemView.findViewById(R.id.foodImage)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.food_item, parent, false)
        return FoodViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: FoodViewHolder, position: Int) {
        val currentItem = menuItems[position]
        holder.foodName.text = currentItem.name
        holder.foodDescription.text = currentItem.description
        holder.foodPrice.text = currentItem.price
        holder.foodImage.setImageResource(currentItem.imageRes)

        // Handle item click event
        holder.itemView.setOnClickListener {
            onItemClick(currentItem)

            // Buka Google Maps saat item diklik
            val gmmIntentUri = Uri.parse(currentItem.googleMapsUrl)
            val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
            mapIntent.setPackage("com.google.android.apps.maps")

            if (mapIntent.resolveActivity(holder.itemView.context.packageManager) != null) {
                holder.itemView.context.startActivity(mapIntent)
            }
        }
    }

    override fun getItemCount(): Int {
        return menuItems.size
    }
}
