package com.example.chall3

data class FoodItem(
    val name: String,
    val price: String,
    val description: String,
    val imageRes: Int,
    val restaurantAddress: String,
    val googleMapsUrl: String = ""
)
