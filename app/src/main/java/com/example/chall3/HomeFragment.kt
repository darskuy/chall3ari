package com.example.chall3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.chall3.FoodAdapter

class HomeFragment : Fragment() {

    private var fragmentMenuListener: FragmentMenuListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        // Inisialisasi RecyclerView
        val recyclerViewMenuGrid: RecyclerView = view.findViewById(R.id.rv_menu_makanan)

        // Inisialisasi data menu makanan Anda
        val menuItems = mutableListOf<FoodItem>()

        menuItems.add(
            FoodItem(
                "Sate Ayam",
                "Rp 40.000",
                "Sate dengan Daging Ayam yang dibalut bumbu kacang",
                R.drawable.sateayam,
                "Alamat Restoran 1",
                "https://maps.app.google.gl/Kd1hbopN2DhnY4DQ9"
            )
        )

        menuItems.add(
            FoodItem(
                "Sate Kambing",
                "Rp 50.000",
                "Sate dengan Daging Kambing yang dibalut bumbu kecap",
                R.drawable.satekambing,
                "Alamat Restoran 2",
                "https://maps.app.google.gl/Kd1hbopN2DhnY4DQ9"
            )
        )

        menuItems.add(
            FoodItem(
                "Dimsum",
                "Rp 20.000",
                "Dimsum dengan berbagai macam isian yang pas sebagai side-dish",
                R.drawable.dimsum,
                "Alamat Restoran 3",
                "https://maps.app.google.gl/Kd1hbopN2DhnY4DQ9"
            )
        )

        menuItems.add(
            FoodItem(
                "Ayam Panggang",
                "Rp 30.000",
                "Ayam Panggang dengan olesan Saus Madu",
                R.drawable.ayampanggang,
                "Alamat Restoran 4",
                "https://maps.app.google.gl/Kd1hbopN2DhnY4DQ9"
            )
        )


        // Adapter untuk RecyclerView
        val adapter = FoodAdapter(menuItems) {

            // Bundle untuk mengirim data ke FragmentDetail
            val args = Bundle()
            args.putString("name", it.name)
            args.putString("price", it.price)
            args.putString("description", it.description)
            args.putInt("imageRes", it.imageRes)
            args.putString("restaurantAddress", it.restaurantAddress)
            args.putString("googleMapsUrl", it.googleMapsUrl)

            // Send Requst untuk menampilkan FragmentDetail
            fragmentMenuListener?.onMenuItemClicked(args)
        }

        val layoutManager = GridLayoutManager(requireContext(), 2) // 2 kolom
        recyclerViewMenuGrid.layoutManager = layoutManager

        // Set adapter ke RecyclerView
        recyclerViewMenuGrid.adapter = adapter

        return view
    }

    interface FragmentMenuListener {
        fun onMenuItemClicked(args: Bundle)
    }

}