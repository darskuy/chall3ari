package com.example.chall3

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.chall3.FoodAdapter
import com.example.chall3.FoodItem
import com.example.chall3.R

class FoodListFragment : Fragment() {
    private lateinit var recyclerView: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_food_list, container, false)
        recyclerView = view.findViewById(R.id.recyclerView)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Initialize your foodList with your data
        val foodList = listOf(
            FoodItem("Burger", "Rp 30.000", "Rp 30.000", R.drawable.burger, "Alamat Restoran 1"),
            FoodItem("Mie Goreng", "Rp 25.000", "Rp 25.000", R.drawable.miegoreng, "Alamat Restoran 2"),
            FoodItem("Snack", "Rp 15.000", "Rp 15.000", R.drawable.snack, "Alamat Restoran 3"),
            FoodItem("Minuman", "Rp 10.000", "Rp 10.000", R.drawable.minuman, "Alamat Restoran 4")
        )

        val foodAdapter = FoodAdapter(foodList) { foodItem ->
            // Handle the click event for a food item
            // You can navigate to the FoodDetailFragment or perform any desired action here
            val args = Bundle()
            args.putString("name", foodItem.name)
            args.putString("price", foodItem.price)
            args.putString("description", foodItem.description)
            args.putInt("imageRes", foodItem.imageRes)
            args.putString("restaurantAddress", "Alamat Restoran")
            args.putString("googleMapsUrl", "https://maps.app.google.gl/Kd1hbopN2DhnY4DQ9")

            // Send Requst untuk menampilkan FragmentDetail
            val fragmentDetails = FragmentDetails()
            fragmentDetails.onMenuItemClicked(args)
        }

        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = foodAdapter
    }
}

